require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe "GET /signin" do
    it "responds with 200" do
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the right template" do
      get :new
      expect(response).to render_template("new")
    end
  end
end
