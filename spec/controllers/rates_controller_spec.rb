require 'rails_helper'

RSpec.describe RatesController, type: :controller do
  describe "get #Index" do
    it "responds with 200" do
      get :index
      expect(response).to be_success
    end

    it "renders the right template" do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe "GET #new" do
    it "responds with 200" do
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the right template" do
      get :new
      expect(response).to render_template('new')
    end
  end
end
