require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "GET #index" do
    it "responds with 200" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the right template" do
      get :index
      expect(response).to render_template("index")
    end

    # I have no idea where does this result come from.
    # I did not create the following user:
    # [#<User id: 298486374, email: "email@email.com", name: "Another user with a different name.", passwor... password_digest: "MyString", created_at: "2016-05-16 16:20:53", updated_at: "2016-05-16 16:20:53">]

    it "returns users if you aren't signed in" do
      user1 = FactoryGirl.create(:user, name: "Test name", email: "test@email.com", password_digest: "password")

      get :index

      expect(assigns(:users)).to match_array(user1)
    end
  end

  describe "GET #new" do
    it "responds with 200" do
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the right template" do
      get :new
      expect(response).to render_template("new")
    end
  end

  describe "POST #create" do
    it "should be a redirect" do
      post :create, :user => {name: "Maciej", email: "test@tester.test.com", password: "password", password_confirmation: "password"}

      expect(response).to have_http_status(302)
    end

    it "should create a new user" do
      post :create, params: {user: {name: "Maciej", email: "test@tester.test.com", password: "password", password_confirmation: "password"}}

      expect(response).to redirect_to(user_path(assigns(:user)))
    end
  end

  describe "Post #delete" do
    it "should be a redirect" do
      delete :destroy, params: {id: 298486374}

      expect(response).to have_http_status(302)
    end
  end
end
