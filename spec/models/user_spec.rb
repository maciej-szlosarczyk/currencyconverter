require 'rails_helper'
require 'factory_girl'

# Tests for User model, only the simple ones.
RSpec.describe User, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:user)).to be_valid
  end

  it "is invalid without email" do
    expect(FactoryGirl.build(:user, email: nil)).to be_valid
  end

  it "is invalid without name" do
    expect(FactoryGirl.build(:user, name: nil)).not_to be_valid
  end

  it "is invalid without password_digest" do
    expect(FactoryGirl.build(:user, password_digest: nil)).not_to be_valid
  end
end
