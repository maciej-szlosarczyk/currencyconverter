require 'rails_helper'
require 'factory_girl'

# Easy tests for Order model.
RSpec.describe Order, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:order)).to be_valid
  end

  it "cannot have a deadline in the past" do
    expect(FactoryGirl.build(:order, deadline: Date.today - 1.year)).not_to be_valid
  end

  it "cannot have a creation date in the future" do
    expect(FactoryGirl.build(:order, created: Date.today + 1.year)).not_to be_valid
  end
end
