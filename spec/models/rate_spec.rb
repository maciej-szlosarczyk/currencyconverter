require 'rails_helper'
require 'factory_girl'

# Basic tests for Rate model.
RSpec.describe Rate, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:rate)).to be_valid
  end

  it "has a unique date" do
    FactoryGirl.create(:rate, date: Date.today - 1.year)
    expect(FactoryGirl.build(:rate, date: Date.today - 1.year)).not_to be_valid
  end
end
