FactoryGirl.define do
  factory :order do |o|
    o.deadline Date.today + 6.months
    o.has 'EUR'
    o.wants 'PLN'
    o.amount '1000'
    o.user User.find_or_create_by(email: 'test@example.com')
  end
end
