FactoryGirl.define do
  factory :rate do |r|
    r.date Date.today - 1.year
  end
end
