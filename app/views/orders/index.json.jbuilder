json.array!(@orders) do |order|
  json.extract! order, :id, :has, :wants, :amount, :deadline
  json.url order_url(order, format: :json)
end
