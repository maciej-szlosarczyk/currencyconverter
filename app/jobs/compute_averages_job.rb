# This job fetches data for given date and a year before,
# and then computes average of those two to be fed to the prediction.
class ComputeAveragesJob < ApplicationJob
  queue_as :default

  def perform(average_record)
    json = make_request(average_record)
    year_ago_json = year_ago_make_request(average_record)
    populate_record(json, year_ago_json, average_record)
  end

  private

  # Makes the request to API, returns JSON object as hash.
  def make_request(average_record)
    # Append request date to request url
    request_url = 'http://api.fixer.io/' + average_record.date.strftime('%Y-%m-%d')

    # Get the response for the first year.
    response = RestClient.get(request_url)
    json = JSON.parse(response)

    json
  end

  # Make the same request, just for a year earlier
  def year_ago_make_request(average_record)
    # Set the date for year ago
    year_ago_date = average_record.date - 1.year
    year_ago_request_url = 'http://api.fixer.io/' + year_ago_date.strftime('%Y-%m-%d')

    # Get the appropriate response
    year_ago_response = RestClient.get(year_ago_request_url)
    year_ago_json = JSON.parse(year_ago_response)

    year_ago_json
  end

  # Populate fields in the database and save it.
  def populate_record(json, year_ago_json, average_record)
    # Populate the date first
    average_record.record_date = json['date']

    # Then, update all the rates.
    puts json['rates']['AUD']
    puts year_ago_json['rates']['AUD']
    puts (json['rates']['AUD'] + year_ago_json['rates']['AUD']) / 2

    average_record.aud = (json['rates']['AUD'] + year_ago_json['rates']['AUD']) / 2
    average_record.bgn = (json['rates']['BGN'] + year_ago_json['rates']['BGN']) / 2
    average_record.brl = (json['rates']['BRL'] + year_ago_json['rates']['BRL']) / 2
    average_record.cad = (json['rates']['CAD'] + year_ago_json['rates']['CAD']) / 2
    average_record.chf = (json['rates']['CHF'] + year_ago_json['rates']['CHF']) / 2
    average_record.cny = (json['rates']['CNY'] + year_ago_json['rates']['CNY']) / 2
    average_record.czk = (json['rates']['CZK'] + year_ago_json['rates']['CZK']) / 2
    average_record.dkk = (json['rates']['DKK'] + year_ago_json['rates']['DKK']) / 2
    average_record.gbp = (json['rates']['GBP'] + year_ago_json['rates']['GBP']) / 2
    average_record.hkd = (json['rates']['HKD'] + year_ago_json['rates']['HKD']) / 2
    average_record.hrk = (json['rates']['HRK'] + year_ago_json['rates']['HRK']) / 2
    average_record.huf = (json['rates']['HUF'] + year_ago_json['rates']['HUF']) / 2
    average_record.idr = (json['rates']['IDR'] + year_ago_json['rates']['IDR']) / 2
    average_record.ils = (json['rates']['ILS'] + year_ago_json['rates']['ILS']) / 2
    average_record.inr = (json['rates']['INR'] + year_ago_json['rates']['INR']) / 2
    average_record.jpy = (json['rates']['JPY'] + year_ago_json['rates']['JPY']) / 2
    average_record.krw = (json['rates']['KRW'] + year_ago_json['rates']['KRW']) / 2
    average_record.mxn = (json['rates']['MXN'] + year_ago_json['rates']['MXN']) / 2
    average_record.myr = (json['rates']['MYR'] + year_ago_json['rates']['MYR']) / 2
    average_record.nok = (json['rates']['NOK'] + year_ago_json['rates']['NOK']) / 2
    average_record.nzd = (json['rates']['NZD'] + year_ago_json['rates']['NZD']) / 2
    average_record.php = (json['rates']['PHP'] + year_ago_json['rates']['PHP']) / 2
    average_record.pln = (json['rates']['PLN'] + year_ago_json['rates']['PLN']) / 2
    average_record.ron = (json['rates']['RON'] + year_ago_json['rates']['RON']) / 2
    average_record.rub = (json['rates']['RUB'] + year_ago_json['rates']['RUB']) / 2
    average_record.sek = (json['rates']['SEK'] + year_ago_json['rates']['SEK']) / 2
    average_record.sgd = (json['rates']['SGD'] + year_ago_json['rates']['SGD']) / 2
    average_record.thb = (json['rates']['THB'] + year_ago_json['rates']['THB']) / 2
    average_record.try = (json['rates']['TRY'] + year_ago_json['rates']['TRY']) / 2
    average_record.usd = (json['rates']['USD'] + year_ago_json['rates']['USD']) / 2
    average_record.zar = (json['rates']['ZAR'] + year_ago_json['rates']['ZAR']) / 2

    # If the record date field is populated, set the fetch as complete and save
    unless average_record.record_date.nil?
      average_record.fetch_completed = true
      average_record.save
    end

  end
end
