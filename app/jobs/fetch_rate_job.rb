# This job gets fired when a rate is created and it does not have the data
# from http://api.fixer.io
class FetchRateJob < ApplicationJob
  queue_as :default

  # Perform the whole operation
  def perform(rate_record)
    json = make_request(rate_record)
    populate_record(json, rate_record)
  end

  private

  # Makes the request to API, returns JSON object as hash.
  def make_request(rate_record)
    # Append request date to request url
    request_url = 'http://api.fixer.io/' + rate_record.date.strftime('%Y-%m-%d')

    # Get the response.
    response = RestClient.get(request_url)
    json = JSON.parse(response)

    unless json['date'].nil?
      response = RestClient.get(request_url)
      json = JSON.parse(response)
    end

    json
  end

  # Populate fields in the database and save it.
  def populate_record(json, rate_record)
    # Populate the date first
    rate_record.record_date = json['date']

    # Then, update all the rates.
    rate_record.update(aud: json['rates']['AUD'], bgn: json['rates']['BGN'],
                       brl: json['rates']['BRL'], cad: json['rates']['CAD'],
                       chf: json['rates']['CHF'], cny: json['rates']['CNY'],
                       czk: json['rates']['CZK'], dkk: json['rates']['DKK'],
                       gbp: json['rates']['GBP'], hkd: json['rates']['HKD'],
                       hrk: json['rates']['HRK'], huf: json['rates']['HUF'],
                       idr: json['rates']['IDR'], ils: json['rates']['ILS'],
                       inr: json['rates']['INR'], jpy: json['rates']['JPY'],
                       krw: json['rates']['KRW'], mxn: json['rates']['MXN'],
                       myr: json['rates']['MYR'], nok: json['rates']['NOK'],
                       nzd: json['rates']['NZD'], php: json['rates']['PHP'],
                       pln: json['rates']['PLN'], ron: json['rates']['RON'],
                       rub: json['rates']['RUB'], sek: json['rates']['SEK'],
                       sgd: json['rates']['SGD'], thb: json['rates']['THB'],
                       try: json['rates']['TRY'], usd: json['rates']['USD'],
                       zar: json['rates']['ZAR'])

    # If the record date field is populated, set the fetch as complete and save
    unless rate_record.record_date.nil?
      rate_record.fetch_completed = true
      rate_record.save
    end
  end
end
