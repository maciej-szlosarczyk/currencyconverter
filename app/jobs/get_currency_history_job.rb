# This job is performed after the user creates a new order.
class GetCurrencyHistoryJob < ApplicationJob
  queue_as :default

  def perform(order_record)
    date_array = calculate_dates(order_record)
    iterate_through_dates(date_array, order_record)
  end

  private

  # Create an array of Wendesdays for the selected date range.
  # Returns an array.
  def calculate_dates(order_record)
    all_dates = []

    history_weeks = ((order_record.created - 1.year)..(order_record.deadline - 1.year)).to_a.select \
     { |k| [3].include?(k.wday) }
    (all_dates << history_weeks).flatten!
    all_dates
  end

  # Go through each date, find or crate Rate record for it.
  # Associate one with the other, and then perform fetch if required.
  def iterate_through_dates(all_dates, order_record)
    all_dates.each do |date|
      a = Average.find_or_create_by(date: date)
      a.orders << order_record
      a.save
      ComputeAveragesJob.perform_now(a) if !a.fetch_completed
    end
  end
end
