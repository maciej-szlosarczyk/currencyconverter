# Orders controller, single, most important part of the application.
class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where user: current_user
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @days = (@order.deadline - @order.created).to_i
    @averages = @order.averages.order('date asc')
    @rate = Rate.find_for_week(@order.created.to_s)

    @averages.each do |average|
      average.computed_rate = average[@order.has.downcase] / average[@order.wants.downcase]
    end

    redirect_to '/signin' if @order.user != current_user
  end

  # GET /orders/new
  def new
    @order = Order.new

    redirect_to '/signin' if current_user.nil?
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)
    @order.user = User.find(current_user)

    respond_to do |format|
      if @order.save
        GetCurrencyHistoryJob.perform_now(@order)
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    @averages = @order.averages.order('date asc')

    respond_to do |format|
      if @order.update(order_params)
        GetCurrencyHistoryJob.perform_now(@order)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Never trust parameters from the scary internet,
  # only allow the white list through.
  def order_params
    params.require(:order).permit(:has, :wants, :amount, :deadline)
  end
end
