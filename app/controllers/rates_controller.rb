# Controller for rates, those are displayed on the index page.
class RatesController < ApplicationController
  # GET /rates
  def index
    @rates = Rate.all.order('date desc')
    @rates.each do |r|
      FetchRateJob.perform_later(r) unless r.fetch_completed
    end
  end

  # GET /rates/{id}
  def show
    @rate = Rate.find(params[:id])
  end

  # Get /rates/new
  def new
    @rate = Rate.new
  end

  # POST /rates/new
  def create
    @rate = Rate.find_for_week(params[:date])
    redirect_to '/rates'
  end

  def set_rate
    @rate = Rate.find_by(params[:id])
  end
end
