# Order model, contains the following elements:
# @deadline - Date set by the user
# @created - Date set automatically to today when created
# @has - String for currency code
# @wants - String for currency code
# @amount - Decimal for the amount of money that the user wants to convert.
class Order < ApplicationRecord
  # Validates if the date is not earlier than today and no later than 5 years
  # from now.
  validate :deadline_not_in_the_past,
           :deadline_not_later_than_1_year,
           :creation_date_not_in_the_future

  validates :amount, presence: true

  # Custom validation options
  # deadline no later than a year from now:
  def deadline_not_later_than_1_year
    if deadline.present? && deadline > Date.today + 1.year
      errors.add(:deadline, 'cannot be later than 1 years from today.')
    end
  end

  # Deadline is not earlier that today.
  def deadline_not_in_the_past
    if deadline.present? && deadline < Date.today
      errors.add(:deadline, 'cannot be in the past')
    end
  end

  def creation_date_not_in_the_future
    if created.present? && created > Date.today
      errors.add(:created, 'cannot be in the future')
    end
  end

  # Belongs to one user
  belongs_to :user

  # Has multiple related averages.
  has_many :averages
  has_and_belongs_to_many :averages
end
