# Model for rates, contains the following elements:
# @date for which was creted.
# @record_date which was returned from fixer API
# @orders that are connected with it
# @fetch_completed, bool that becomes true once the fetch is done.
# Multiple numerical value, one for each currency
class Rate < ApplicationRecord
  validates :date, uniqueness: true

  has_many :orders
  has_and_belongs_to_many :orders

  # Define a computed rate that is set on every order
  attr_accessor :computed_rate


  # Find unique record for that particular week
  def self.find_for_week(date_string)
    date = Date.parse(date_string)
    # Get to the beginning of a week, then add 3 for Wednesday
    wednesday = date - date.wday + 3
    puts wednesday
    record = Rate.first_or_create(date: wednesday)

    FetchRateJob.perform_later(record) unless record.fetch_completed

    record
  end

  # This job is similar to the one above, but accepts date objects straight
  # from the Order class.
  def self.find_history(date)
    record = Rate.where(date: date)

    record.create(date: date) if record.nil?

    FetchRateJob.perform_later(record) if record.fetch_complete == false
  end
end
