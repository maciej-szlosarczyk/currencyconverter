# Average model, comprises of 2 currency history weeks where year = n and year = n-1.
# In principle, it is very similar to the Rate model.
class Average < ApplicationRecord
  validate :cannot_be_in_future
  validates :date, uniqueness: true

  has_many :orders
  has_and_belongs_to_many :orders

  # Define a computed rate that is set on every order
  attr_accessor :computed_rate

  # Throw an error if the date is later than today.
  # Otherwise, the average will only take into account one number, and would not be an average.
  def cannot_be_in_future
    if date > Date.today
      errors.add(:date, "Cannot be in the future.")
    end
  end

  # Find unique record for that particular week
  def self.find_for_week(date_string)
    date = Date.parse(date_string)
    # Get to the beginning of a week, then add 3 for Wednesday
    wednesday = date - date.wday + 3
    record = Average.find_or_create_by(date: wednesday)

    ComputeAveragesJob.perform_later(record) unless record.fetch_completed
  end

  # This job is similar to the one above, but accepts date objects straight
  # from the Order class.
  def self.find_history(date)
    record = Rate.where(date: date)
    record.create(date: date) if record.nil?
    ComputeAveragesJob.perform_later(record) if record.fetch_complete == false
  end
end
