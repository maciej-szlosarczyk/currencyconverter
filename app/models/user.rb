# NOTE: Uses a very simple authentication mechanism built
# around has_secure_password due to
# the following issue in Authlogic with Rails 5:
# https://github.com/binarylogic/authlogic/issues/487
# Needs to be rewritten when a fix becomes available.
class User < ApplicationRecord
  validates :email, uniqueness: true
  validates :name, :password_digest, presence: true

  # Use bcrypt for password hashing
  has_secure_password

  # A user can have many orders:
  has_many :orders, dependent: :destroy
end
