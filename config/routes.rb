Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #CRUD for users
  resources :users


  #Create user account
  get '/signup' => 'users#new'
  post '/signup' => 'users#create'
  get '/user' => 'users#show'

  # CRUD for orders.
  resources :orders

  # Create or destroy a user session
  get '/signin' => 'sessions#new'
  post '/signin' => 'sessions#create'
  get '/signout' => 'sessions#destroy'

  # Rates
  get '/rates' => 'rates#index'
  get '/rates/new' => 'rates#new'
  post '/rates' => 'rates#create'
  get '/rates/:id' => 'rates#show'

  # Index route
  get '/' => 'rates#index'
end
