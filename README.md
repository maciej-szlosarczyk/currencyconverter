# README

## Live demo

Demo is available at the following url:

[https://fierce-journey-69511.herokuapp.com/](https://fierce-journey-69511.herokuapp.com/)

## Known limitations:

* In some cases, the graph does not load properly at the first time, but after reload works.
* The rank support is spotty. For some orders, it computes them properly, for others it is all over the place.
* The user creation takes you to the page where you can see all the users, not only yourself.
* Jobs are blocking the execution, which works for the demo, but will not work in production at any level.
* Since Authlogic does not work properly, the session mechanism is a dirty hack.


## Areas of improvement:

* The order view is definitely too fat. It should either be broken down. At the same time, most of its elements should not be stored in the database, because that is also quite expensive. Should think of a way to improve that.
* RSpec tests are clumsy, write better ones once you learn them properly.
* Add retry logic to jobs, maybe add some backend to keep history (Redis)

