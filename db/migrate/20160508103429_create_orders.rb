class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :has
      t.string :wants
      t.decimal :amount
      t.date :deadline
      t.date :created, default: Date.today

      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
