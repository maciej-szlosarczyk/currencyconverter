class CreateRates < ActiveRecord::Migration[5.0]
  def change
    create_table :rates do |t|
      # Date for which the rates is registered
      t.datetime :date, index: true

      # These come from the fixer.io API:
      t.datetime :record_date
      # Euro defaults to 1.
      t.decimal :eur, default: 1
      t.decimal :aud
      t.decimal :bgn
      t.decimal :brl
      t.decimal :cad
      t.decimal :chf
      t.decimal :cny
      t.decimal :czk
      t.decimal :dkk
      t.decimal :gbp
      t.decimal :hkd
      t.decimal :hrk
      t.decimal :huf
      t.decimal :idr
      t.decimal :ils
      t.decimal :inr
      t.decimal :jpy
      t.decimal :krw
      t.decimal :mxn
      t.decimal :myr
      t.decimal :nok
      t.decimal :nzd
      t.decimal :php
      t.decimal :pln
      t.decimal :ron
      t.decimal :rub
      t.decimal :sek
      t.decimal :sgd
      t.decimal :thb
      t.decimal :try
      t.decimal :usd
      t.decimal :zar

      # Related to jobs:
      t.boolean :fetch_completed, default: false

      t.timestamps
    end
  end
end
