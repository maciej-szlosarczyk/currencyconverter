# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519073540) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "averages", force: :cascade do |t|
    t.datetime "date"
    t.datetime "record_date"
    t.decimal  "eur",             default: "1.0"
    t.decimal  "aud"
    t.decimal  "bgn"
    t.decimal  "brl"
    t.decimal  "cad"
    t.decimal  "chf"
    t.decimal  "cny"
    t.decimal  "czk"
    t.decimal  "dkk"
    t.decimal  "gbp"
    t.decimal  "hkd"
    t.decimal  "hrk"
    t.decimal  "huf"
    t.decimal  "idr"
    t.decimal  "ils"
    t.decimal  "inr"
    t.decimal  "jpy"
    t.decimal  "krw"
    t.decimal  "mxn"
    t.decimal  "myr"
    t.decimal  "nok"
    t.decimal  "nzd"
    t.decimal  "php"
    t.decimal  "pln"
    t.decimal  "ron"
    t.decimal  "rub"
    t.decimal  "sek"
    t.decimal  "sgd"
    t.decimal  "thb"
    t.decimal  "try"
    t.decimal  "usd"
    t.decimal  "zar"
    t.boolean  "fetch_completed", default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["date"], name: "index_averages_on_date", using: :btree
  end

  create_table "averages_orders", id: false, force: :cascade do |t|
    t.integer "order_id"
    t.integer "average_id"
    t.index ["average_id"], name: "index_averages_orders_on_average_id", using: :btree
    t.index ["order_id"], name: "index_averages_orders_on_order_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.string   "has"
    t.string   "wants"
    t.decimal  "amount"
    t.date     "deadline"
    t.date     "created",    default: '2016-05-15'
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["user_id"], name: "index_orders_on_user_id", using: :btree
  end

  create_table "orders_rates", id: false, force: :cascade do |t|
    t.integer "order_id"
    t.integer "rate_id"
    t.index ["order_id"], name: "index_orders_rates_on_order_id", using: :btree
    t.index ["rate_id"], name: "index_orders_rates_on_rate_id", using: :btree
  end

  create_table "rates", force: :cascade do |t|
    t.datetime "date"
    t.datetime "record_date"
    t.decimal  "eur",             default: "1.0"
    t.decimal  "aud"
    t.decimal  "bgn"
    t.decimal  "brl"
    t.decimal  "cad"
    t.decimal  "chf"
    t.decimal  "cny"
    t.decimal  "czk"
    t.decimal  "dkk"
    t.decimal  "gbp"
    t.decimal  "hkd"
    t.decimal  "hrk"
    t.decimal  "huf"
    t.decimal  "idr"
    t.decimal  "ils"
    t.decimal  "inr"
    t.decimal  "jpy"
    t.decimal  "krw"
    t.decimal  "mxn"
    t.decimal  "myr"
    t.decimal  "nok"
    t.decimal  "nzd"
    t.decimal  "php"
    t.decimal  "pln"
    t.decimal  "ron"
    t.decimal  "rub"
    t.decimal  "sek"
    t.decimal  "sgd"
    t.decimal  "thb"
    t.decimal  "try"
    t.decimal  "usd"
    t.decimal  "zar"
    t.boolean  "fetch_completed", default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["date"], name: "index_rates_on_date", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["email"], name: "index_users_on_email", using: :btree
  end

end
